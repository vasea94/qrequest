$(document).ready(function () {
   $('.side-nav > li > a').click(function () {
        $(this).toggleClass('collapsed-in');
   });
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

        $('.dataTable').DataTable();
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "3000",
    "hideDuration": "3000",
    "timeOut": "3000",
    "extendedTimeOut": "3000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

$(document).on('change', ':input', function () {
    if($(this).hasClass('error'))
        $(this).removeClass('error');
});
function saveForm(parentThat) {
    var form = $('#'+ $(parentThat).data('form-id'));

    var serializedForm = $(form).find("select, textarea, input").serialize();

    if (!$(form)) {
        toastr.error('Error! Please contact administrator');
        return;
    }
    $.ajax({
            method: "POST",
            url: $(form).attr('action'),
            data: serializedForm
        })
        .done(function (response) {
            if(response.messages == null)
                return;
            var ObjNames = Object.keys(response.messages);
            for(var messageKeyIterator in ObjNames){
                $(form).find("[name='"+ObjNames[messageKeyIterator]+"']").addClass('error');
            }
            if(response.status == true)
                for (var messageIterator in response.messages) {

                        toastr.success(response.messages[messageIterator]);
                }
            else
                for (var messageIterator in response.messages) {
                toastr.error(response.messages[messageIterator]);
             }

            if (response.redirect) {
                window.location = response.redirect;
            }

            if (response.itemId) {

                $(form).find('[name="item_id"]').val(response.itemId);

                $(form).submit();
            }

        })
    .fail(function (msg) {
        toastr.error('Fail to send data');
    });

}

/*
* Helper input_file
*
* file dynamic uploader, and customizer
* glyphicon-refresh - class for animation
* */

$(document).ready(function () {
    var inputFilesFormat = ".file-div input[type='hidden']";
    $(inputFilesFormat ).each(function (iterator, parentThat) {
        var htmlFormat = "<div class='none'>" +
            "<form action='" + $(parentThat).data('url')+ "' enctype='multipart/form-data' class='upload-form' >" +
                "<input type='file' name='" + $(parentThat).attr('name')+ "'/>" +
            "</form>" +
            "</div>";
        $('body').append(htmlFormat);
    });
    $(document).on('click','.file-div button', function (parentThat) {
        parentThat.preventDefault();

        //$(this).find('span').addClass('glyphicon-refresh');

        var inputName = $(this).closest('div').find('input').attr('name');
        var clonedFileInput = $('input[name='+ inputName +'][type="file"]');
        $(clonedFileInput).trigger('click');

    });

        $(document).on('change','input[type=file]', function  () {
            var parentThat = $(this);
            setTimeout(function(){
                $(parentThat).closest('form').submit();
            }, 500);
        });

    $(document).on('submit', '.upload-form', function(e){
        e.preventDefault();

        var inputName = $(this).find('input').attr('name');
        var clonedTextHidenInput = $('input[name='+ inputName +'][type="hidden"]');
        $(clonedTextHidenInput).closest('div').find('span').addClass('glyphicon-refresh');

        if($(this).find('input').val() == '') {
            $(clonedTextHidenInput).closest('div').find('span').removeClass('glyphicon-refresh');
            return;
        }
        var formData = new FormData($(this)[0]);
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            async: true,
            success: function (data) {
                $(clonedTextHidenInput).closest('div').find('img').remove();

                for(var i in data[0].fileName){
                $(clonedTextHidenInput).val(data[0].url[i]);
                if(data.fileType = 'img'){
                    $(clonedTextHidenInput).closest('div').append("<img src='" + data[0].url[i] + "' width='70' height='70'/></div>");
                    $(clonedTextHidenInput).closest('div').find('span').removeClass('glyphicon-refresh');
                }
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });

});

