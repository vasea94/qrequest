<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Institution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->string('description', 255);
            $table->string('logo', 255);
            $table->string('address', 255);
            $table->string('phone', 64);
            $table->string('email', 64);
            $table->string('fax', 64);
            $table->integer('country_id');
            $table->integer('district_id');
            $table->integer('locality_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
