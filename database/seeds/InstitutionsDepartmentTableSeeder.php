<?php

use Illuminate\Database\Seeder;

class InstitutionsDepartmentTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('institutions_department')->delete();
        
	}

}
