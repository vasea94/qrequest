<?php

use Illuminate\Database\Seeder;

class FunctionsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('functions')->delete();
        
		\DB::table('functions')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'Secretariat',
				'description' => 'Functionarii secretariat ',
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '2015-12-02 02:07:59',
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'Decanat',
				'description' => 'Functionarii decanat',
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '2015-12-02 02:06:53',
			),
		));
	}

}
