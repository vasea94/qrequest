<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('country')->delete();
        
	}

}
