<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('users')->delete();
        
		\DB::table('users')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'administrator',
				'email' => 'administrator@qrequest.dev',
				'password' => '$2y$10$rAxaNgphOOQC2cgE7J62C.IORU47YmOFtGljGOXDmaAb0rvsvL6qm',
				'user_role_id' => 1,
				'profile_id' => 1,
				'institution_id' => 1,
				'remember_token' => '',
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '0000-00-00 00:00:00',
				'active' => 2,
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'user',
				'email' => 'user@qrequest.dev',
				'password' => '$2y$10$IPtzMr48HBjvnGUu8IEwCOIzlyWhzuYbWga8Wc3Q4GKFHQW4X41OW',
				'user_role_id' => 0,
				'profile_id' => 0,
				'institution_id' => 0,
				'remember_token' => '',
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '2015-12-02 07:05:24',
				'active' => 1,
			),
		));
	}

}
