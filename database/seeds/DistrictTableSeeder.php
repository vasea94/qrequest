<?php

use Illuminate\Database\Seeder;

class DistrictTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('district')->delete();
        
	}

}
