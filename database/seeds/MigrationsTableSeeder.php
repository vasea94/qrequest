<?php

use Illuminate\Database\Seeder;

class MigrationsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('migrations')->delete();
        
		\DB::table('migrations')->insert(array (
			0 => 
			array (
				'migration' => '2014_10_12_000000_department',
				'batch' => 1,
			),
			1 => 
			array (
				'migration' => '2014_10_12_000000_departments_function',
				'batch' => 1,
			),
			2 => 
			array (
				'migration' => '2014_10_12_000000_entitie',
				'batch' => 1,
			),
			3 => 
			array (
				'migration' => '2014_10_12_000000_entity_action',
				'batch' => 1,
			),
			4 => 
			array (
				'migration' => '2014_10_12_000000_functions',
				'batch' => 1,
			),
			5 => 
			array (
				'migration' => '2014_10_12_000000_institution',
				'batch' => 1,
			),
			6 => 
			array (
				'migration' => '2014_10_12_000000_institutions_department',
				'batch' => 1,
			),
			7 => 
			array (
				'migration' => '2014_10_12_000000_user',
				'batch' => 1,
			),
			8 => 
			array (
				'migration' => '2014_10_12_000000_user_department',
				'batch' => 1,
			),
			9 => 
			array (
				'migration' => '2014_10_12_000000_user_function',
				'batch' => 1,
			),
			10 => 
			array (
				'migration' => '2014_10_12_000000_user_role',
				'batch' => 1,
			),
			11 => 
			array (
				'migration' => '2014_10_12_000000_users_institution',
				'batch' => 1,
			),
			12 => 
			array (
				'migration' => '2014_10_12_100000_create_password_resets_table',
				'batch' => 1,
			),
			13 => 
			array (
				'migration' => '2014_10_12_000000_country',
				'batch' => 2,
			),
			14 => 
			array (
				'migration' => '2014_10_12_000000_district',
				'batch' => 2,
			),
			15 => 
			array (
				'migration' => '2014_10_12_000000_locality',
				'batch' => 2,
			),
		));
	}

}
