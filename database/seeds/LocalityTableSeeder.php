<?php

use Illuminate\Database\Seeder;

class LocalityTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('locality')->delete();
        
	}

}
