<?php

use Illuminate\Database\Seeder;

class DepartmentsFunctionTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('departments_function')->delete();
        
	}

}
