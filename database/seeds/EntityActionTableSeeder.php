<?php

use Illuminate\Database\Seeder;

class EntityActionTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('entity_action')->delete();
        
	}

}
