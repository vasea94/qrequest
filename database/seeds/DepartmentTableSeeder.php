<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('department')->delete();
        
		\DB::table('department')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'Fizica si Inginerie',
				'description' => 'Departament Fizica si inginerie',
				'user_role_id' => 0,
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '2015-12-02 05:46:40',
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'Jurnalism si stiinte ale Cominicarii',
				'description' => 'J si TV',
				'user_role_id' => 0,
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '2015-12-02 02:08:53',
			),
		));
	}

}
