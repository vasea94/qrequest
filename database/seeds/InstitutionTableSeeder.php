<?php

use Illuminate\Database\Seeder;

class InstitutionTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('institution')->delete();
        
		\DB::table('institution')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'USM',
				'description' => 'Universitatea de Stat din Moldova',
				'logo' => '/images/81705.jpg',
				'address' => 'address',
				'phone' => '+373',
				'email' => 'usm@usm.md',
				'fax' => '+373-2',
				'country_id' => 1,
				'district_id' => 1,
				'locality_id' => 1,
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '0000-00-00 00:00:00',
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'USMF',
				'description' => 'Univ de Farmacie',
				'logo' => '/images/37586.jpg',
				'address' => 'str Univ de Farm',
				'phone' => '+333',
				'email' => 'usmf@usm.md',
				'fax' => '+444',
				'country_id' => 1,
				'district_id' => 1,
				'locality_id' => 1,
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '0000-00-00 00:00:00',
			),
		));
	}

}
