<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);

        Model::reguard();
    	$this->call('UsersTableSeeder');
		$this->call('CountryTableSeeder');
		$this->call('DepartmentTableSeeder');
		$this->call('DepartmentsFunctionTableSeeder');
		$this->call('DistrictTableSeeder');
		$this->call('EntitieTableSeeder');
		$this->call('EntityActionTableSeeder');
		$this->call('FunctionsTableSeeder');
		$this->call('InstitutionTableSeeder');
		$this->call('InstitutionsDepartmentTableSeeder');
		$this->call('LocalityTableSeeder');
		$this->call('MigrationsTableSeeder');
		$this->call('PasswordResetsTableSeeder');
	}
}
