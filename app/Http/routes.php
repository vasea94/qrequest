<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('auth/login', 'Auth\CustomAuthController@loginAndRegister');
Route::post('auth/login', 'Auth\CustomAuthController@checkLogin');
Route::post('auth/register', 'Auth\CustomAuthController@checkRegister');


Route::get('/home',function(){
    return redirect('/');
});

Route::any('/upload', 'FileController@upload');

Route::group([
    'namespace' => 'Actions',
    'middleware' => 'auth',
], function(){
    Route::get('/','DefaultController@index');
});



Route::group([
    'namespace' => 'Admin',
    'prefix'=>'admin',
    'middleware' => 'admin',
], function(){
    Route::get('/','DefaultController@index');
    Route::get('/institution','InstitutionController@index');
    Route::get('/institution/add/{id?}','InstitutionController@add');
    Route::post('/institution/save/{id?}','InstitutionController@save');

    Route::get('/department','DepartmentController@index');
    Route::get('/department/add/{id?}','DepartmentController@add');
    Route::post('/department/save/{id?}','DepartmentController@save');

    Route::get('/function','FunctionController@index');
    Route::get('/function/add/{id?}','FunctionController@add');
    Route::post('/function/save/{id?}','FunctionController@save');


    Route::get('/action','ActionController@index');
    Route::get('/action/add/{id?}','ActionController@add');

    Route::get('/user','UserController@index');
    Route::get('/user/add/{id?}','UserController@add');
    Route::post('/user/save/{id?}','UserController@save');
});


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

// to do: in development mode
Route::any('{lang}/{department}/{function}/{route?}', 'RoleManager@routeResponder')
    ->where('lang','(ro|ru|en)' );