<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App;
class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->user_role_id == 1)
            return $next($request);
        else
            return redirect('/');
//            return App::abort('404');
    }

}
