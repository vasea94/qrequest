<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 11/25/2015
 * Time: 12:51 PM
 */

namespace App\Http\Controllers;


//use App\Http\Controllers\Actions\RequestController;

class ClassDetector
{

    public $classes = [];
    public $namespace = '\App\Http\Controllers\Actions\\';
    public $possiblesClasses = [];

    public function __construct($path = null, $step = 1)
    {
        $this->path = $path;
        $this->absolutePath = base_path($path);
        $this->step = $step;
        $this->setFiles();
        $this->setClasses();
    }

    public function setFiles(){
        $scanFiles = scandir($this->absolutePath);
        if(count($scanFiles))
            foreach($scanFiles as $scanFile)
                if(!in_array($scanFile, ['.', '..']))
//          we eliminate the extension, .php(4 chars), will be and update for optimization
                    $this->possiblesClasses[] = substr($scanFile, 0, strlen($scanFile) - 4);
    }

    public function filterOnlyClasses(){
        foreach($this->possiblesClasses as $checkedClass){
            $reflection = new \ReflectionClass($this->namespace.$checkedClass);
            $methods = [];
            foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {

                if ($method->class == $reflection->getName()) {

                    $comment = $method->getDocComment();
                    if($comment)
                    {
                        preg_match_all('/@(.+)\s*:\s*(.+)\n/', $comment, $annotations);

                        /*
                         * $annotations[0] = @anotation: value\r\n
                         * $annotations[1] = anotation
                         * $annotations[2] = value\r
                         *
                         * */

                        $vars = array_combine($annotations[1], $annotations[2]);
                        $vars = array_map(function($item){
                            return rtrim($item, "\r\n");
                        }, $vars);

                    }

                }
            }
        }
    }

    public function getFiles(){
        return $this->possiblesClasses;
    }

    public function getAbsolutePath(){
        return $this->absolutePath;
    }
    public function getPath(){
        return $this->path;
    }

    public function setClasses(){

    }







}