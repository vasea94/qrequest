<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 12/5/2015
 * Time: 10:28 AM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Entitie;

class ActionController extends Controller
{
    public function index()
    {
        $view = 'admin.action.index';
        $viewVariables['items'] = Entitie::with([
            'actions'
        ])
            ->get();


        return view($view, $viewVariables);
    }

    public function add(){
        $view = 'admin.action.add';

        $viewVariables['items'] = Entitie::with([
            'actions'
        ])
            ->get();


        return view($view, $viewVariables);
    }

}