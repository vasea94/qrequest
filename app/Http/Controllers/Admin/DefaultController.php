<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 11/26/2015
 * Time: 2:55 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\RoleManager;
use Auth;

class DefaultController extends RoleManager
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index(){
        $view = 'admin.welcome';

        return view($view);
    }



}