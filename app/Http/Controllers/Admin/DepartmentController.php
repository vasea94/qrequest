<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 11/22/2015
 * Time: 11:11 PM
 */

namespace App\Http\Controllers\Admin;

use Validator;
use Input;
use App\Http\Controllers\RoleManager;
use App\Models\Department;

class DepartmentController extends RoleManager
{
    public function index()
    {
        $view = 'admin.department.index';
        $viewVariables['departments'] = Department::get();
        return view($view, $viewVariables);
    }

    public function add($id = null){
        $view = 'admin.department.add';
        $viewVariables = [];
        if(!is_null($id)){
            $viewVariables['item'] = Department::find($id);
        }

        return view($view, $viewVariables);
    }

    public function save($id = null){
        $messages = array();
        $item = Validator::make(Input::all(), [
            'name' => 'required',
            'description'=>'required',
//            'logo'=>'required',
//            'phone'=>'required',
//            'email'=>'required',
//            'fax'=>'required',
//            'country_id'=>'required|numeric',
//            'district_id'=>'required|numeric',
//            'locality_id'=>'required|numeric',
//            'address'=>'required'
        ]);

        if($item->fails() )
            $messages = $item->messages();

        if(count($messages))
            return response()->json([
                'status' => false,
                'messages' => $messages,
            ]);

        $data = array_filter([
            'name' => Input::get('name'),
            'description'=>Input::get('description'),
//                'logo'=>Input::get('logo'),
//                'phone'=>Input::get('phone'),
//                'email'=>Input::get('email'),
//                'fax'=>Input::get('fax'),
//                'country_id'=>Input::get('country_id'),
//                'district_id'=>Input::get('district_id'),
//                'locality_id'=>Input::get('locality_id'),
//                'address'=>Input::get('address')
        ]);
        if(!is_null($id)) {
            Department::where('id',$id)->update($data);
        }else {
            $id = @Department::insert($data)['id'];
        }

        return response()->json([
            'status' => true,
            'messages' => ['Save'],
        ]);
    }

}