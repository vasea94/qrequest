<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 11/22/2015
 * Time: 11:11 PM
 */

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Validator;
use Input;
use App\Http\Controllers\RoleManager;

class UserController extends RoleManager
{
    public function index()
    {
        $view = 'admin.user.list';
        $viewVariables['items'] = User::get();
        return view($view, $viewVariables);
    }

    public function add($id = null){
        $view = 'admin.user.add';
        $viewVariables = [];
        if(!is_null($id)){
            $viewVariables['item'] = User::find($id);
        }

        return view($view, $viewVariables);
    }

    public function save($id = null){
        $messages = array();
        $item = Validator::make(Input::all(), [
            'name' => 'required',
            'email'=>'required|email',
//            'password'=>'required|min:6',
            'active'=>'required|numeric'
        ]);

        if($item->fails() )
            $messages = $item->messages();

        if(count($messages))
            return response()->json([
                'status' => false,
                'messages' => $messages,
            ]);
        $data = array_filter([
            'name' => Input::get('name'),
            'email'=>Input::get('email'),
            'active'=>@Input::get('active') ,
            'password'=>(@Input::get('password') == null)? null  : bcrypt(Input::get('password')),
        ]);
        if(!is_null($id)) {
            User::where('id',$id)->update($data);
        }else {
            $id = @User::insert($data)['id'];
        }

        return response()->json([
            'status' => true,
            'messages' => ['Save'],
            'redirect'=> url('/admin/user/')
        ]);
    }

}