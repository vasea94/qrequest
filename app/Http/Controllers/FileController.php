<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 12/1/2015
 * Time: 11:29 PM
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Input;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function upload(){

        $response = [];
        $key = 0;
        foreach(Input::file() as $singleFile)
        {
            $extension = $singleFile->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renameing image
            $destinationPath = 'uploads';
            $fileType = '';
            switch(strtolower($singleFile->getClientOriginalExtension())){
                case 'jpg':
                case 'png':
                case 'jpeg':{
                    $fileType = 'img';
                    $destinationPath = 'images';
                    break;
                }
                case 'doc':
                case 'docx':
                case 'xls':
                case 'pdf':{
                    $fileType = 'doc';
                    $destinationPath = 'docs';
                    break;
                }
                default : {
                    $destinationPath = 'uploads';
                    $fileType = 'other';
                    break;
                }
            }

            $singleFile->move($destinationPath, $fileName);

            $response['fileName'][$key] = $fileName;
            $response['fileType'][$key] = $fileType;
            $response['url'][$key] = '/'.$destinationPath.'/'.$fileName;
            $key++;
        }
        return response()->json([
            $response,
            'success'=>true
        ]);
    }

}