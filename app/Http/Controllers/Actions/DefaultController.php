<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 11/26/2015
 * Time: 2:55 PM
 */

namespace App\Http\Controllers\Actions;

use Auth;
use App\Http\Controllers\Controller;

class DefaultController extends Controller
{

    public function index(){
        $view = 'admin.welcome';

        return view($view);
    }

}