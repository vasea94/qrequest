<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 2/6/2016
 * Time: 12:00 PM
 */

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Models\Institution;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use Request;
use Validator;
use Illuminate\Support\Facades\Hash;

class CustomAuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function loginAndRegister(){
        if(Request::isMethod('post'))
            return $this->checkLogin();

        $view = 'auth.loginAndRegister';

        $institutions = Institution::with([
            'departments'
        ])->get();
//dd($institutions);
        return view($view, get_defined_vars());
    }

    public function checkLogin(){
        $messages = array();
        $item = Validator::make(Input::all(), [
            'email' => 'required',
            'password'=>'required|min:4',
        ]);

        if($item->fails() )
            $messages = $item->messages();

        if(count($messages))
            return response()->json([
                'status' => false,
                'messages' => $messages,
            ]);

        if (!Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'))))
            return response()->json([
                'status' => false,
                'messages' => [trans('variables.user_not_exist')],
            ]);


        switch(Auth::user()->active){
            case  false : {
                Auth::logout();
                return response()->json([
                    'status' => false,
                    'messages' => [trans('variables.user_not_active')],
                ]);
                break;
            }
            default :{
                return response()->json([
                    'status' => true,
                    'messages' => [trans('variables.login_success_full')],
                    'redirect' =>  url('home'),
                ]);
            }
        }

    }
    public function checkRegister(){
        return 'was';

    }

}