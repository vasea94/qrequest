<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class District
 */
class District extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'name',
        'prefix',
        'country_id'
    ];

    protected $guarded = [];

        
}