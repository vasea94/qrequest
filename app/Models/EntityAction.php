<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EntityAction
 */
class EntityAction extends Model
{
    protected $table = 'entity_action';

    public $timestamps = true;

    protected $fillable = [
        'entity_id',
        'name',
        'description',
        'short_name'
    ];

    protected $guarded = [];

        
}