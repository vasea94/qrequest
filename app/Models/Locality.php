<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Locality
 */
class Locality extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'name',
        'prefix',
        'district_id'
    ];

    protected $guarded = [];

        
}