<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Institution
 */
class Institution extends Model
{

    protected $table = 'institution';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'logo',
        'address',
        'phone',
        'email',
        'fax',
        'country_id',
        'district_id',
        'locality_id'
    ];

    public function departments(){
        return $this->belongsToMany(
            'App\Models\Department',
            'institutions_department',
            'institution_id',
            'department_id');
    }

    protected $guarded = [];

        
}