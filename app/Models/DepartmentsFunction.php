<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DepartmentsFunction
 */
class DepartmentsFunction extends Model
{

    protected $table = 'departments_function';
    public $timestamps = true;

    protected $fillable = [
        'department_id',
        'function_id'
    ];

    protected $guarded = [];

        
}