<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 */
class Country extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'name',
        'prefix'
    ];

    protected $guarded = [];

        
}