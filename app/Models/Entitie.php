<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Entitie
 */
class Entitie extends Model
{

    protected $table = 'entitie';
    public $timestamps = true;

    protected $fillable = [
        'active',
        'name',
        'description',
        'short_name'
    ];

    protected $guarded = [];

    public function actions(){
        return $this->hasMany('App\Models\EntityActions', 'entity_id', 'id');
    }
}