<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserDepartment
 */
class UserDepartment extends Model
{
    protected $table = 'user_department';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'department_id'
    ];

    protected $guarded = [];

    public function scopeFilteredByUser( $q, $id = null){
        return $q->where('user_id', $id);
    }
        
}