<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersInstitution
 */
class UsersInstitution extends Model
{
    protected $table = 'users_institution';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'institution_id'
    ];

    protected $guarded = [];

        
}