<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InstitutionsDepartment
 */
class InstitutionsDepartment extends Model
{
    protected $table = 'institutions_department';

    public $timestamps = true;

    protected $fillable = [
        'institution_id',
        'department_id'
    ];

    protected $guarded = [];

        
}