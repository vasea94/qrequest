<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Function
 */
class Functions extends Model
{

    public $timestamps = true;

    protected $table = 'functions';
    protected $fillable = [
        'name',
        'description'
    ];

    protected $guarded = [];

        
}