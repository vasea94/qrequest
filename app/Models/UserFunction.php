<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserFunction
 */
class UserFunction extends Model
{
    protected $table = 'user_function';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'function_id'
    ];

    protected $guarded = [];

    public function scopeFilteredByUser( $q, $id = null){
        return $q->where('user_id', $id);
    }
        
}