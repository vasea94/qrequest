<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Department
 */
class Department extends Model
{

    protected $table = 'department';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'user_role_id'
    ];

    public function scopeForAdmin($q){
        return $q->where('user_role_id', 1);
    }

    public function scopeForUser($q){
        return$q->where('user_role_id','!=', 1);
    }

    public function functions(){
        return $this->belongsToMany(
            'App\Models\Functions',
            'departments_function',
            'department_id',
            'function_id');
    }


        
}