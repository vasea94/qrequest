<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 */
class User extends Model
{
    protected $table = 'users';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'email',
        'password',
        'user_role_id',
        'profile_id',
        'institution_id',
        'remember_token',
        'active'
    ];

    public function institution(){
        return $this->hasOne('App\Models\Institution', 'id', 'institution_id');
    }


        
}