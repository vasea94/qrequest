<?php
/**
 * Created by PhpStorm.
 * User: mpc
 * Date: 11/26/2015
 * Time: 5:13 PM
 */

namespace App\Providers;


use App\Models\User;
use App\Models\UserDepartment;
use App\Models\UserFunction;
use Auth;
use App\Models\Department;
use Illuminate\Support\ServiceProvider;

class DefineDepartmentsAndFunctions extends ServiceProvider
{

    public function register()
    {

        // TODO: Implement register() method.
    }

    public function boot(){

        $this->app['view']->composer('app', function($view) {

//            if is login
            if (Auth::check()){
            $institution = User::find(Auth::user()->id)
                ->institution()
                ->with([
                    'departments' => function ($q) {
                        return $q->whereIn('department.id', array_column(UserDepartment::FilteredByUser(Auth::user()->id)
                                ->get(['department_id'])
                                ->toArray(), 'department_id')
                        );
                    },
                    'departments.functions'=> function ($q) {
                        return $q->whereIn('functions.id', array_column(UserFunction::FilteredByUser(Auth::user()->id)
                                ->get(['function_id'])
                                ->toArray(), 'function_id')
                        );
                    },
                ])
                ->first();
        }
        else
//            todo : I take a break to think
            $institution = [];

            $view->institution = $institution;
        });

    }

}