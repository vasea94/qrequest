<div class='file-div'>
    <button class='btn btn-sm'>
        <span class='glyphicon glyphicon-refresh-animate'></span>
        Select file
    </button>
    <input type="hidden" name="{{$name or 'file'}}" data-url="{{$url or '/upload'}}" value=""/>
</div>
