<div class="setting-panel">
    <span href="#" class="title" disabled>{{$title or '' }}</span>
    @if(isset($actions) && count($actions))
        @foreach($actions = array_reverse($actions)  as $action)
            <a href="{{$action['url'] or '#'}}" class="btn  pull-right settings form-{{$action['type'] or 'plus'}}" onclick="{{$action['type']}}Form(this)">
                <i class="fa fa-{{$action['type'] or 'plus'}}"></i>
                {{trans('variables.'.$action['type'])}}
            </a>
        @endforeach
    @endif
</div>