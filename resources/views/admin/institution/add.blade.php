@extends('app')
@section('breadcrumbs')
    @include('helpers.breadcrumbs', [
        'actions'=>[
            [
                'name'=>'Add',
                'url'=>'add',
                'type'=>'add'
            ],
            [
                'name'=>'Save',
                // 'url'=>'save',
                'type'=>'save'
            ]
        ],
        'title'=>'Administer Institutions'
    ])
@endsection
@section('content')
<form action="{{url('/admin/institution/save/'.(isset($item->id)?$item->id : ''))}}" method="post"
      class="form-save" role="form">
    <div class="row col-lg-6 col-sm-12 form-block">
        <div class="row col-lg-12">
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Institution name</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <input type="text" class="form-control" id="name" value="{{$item->name or ''}}" name="name" placeholder="{{trans('variables.enter_name')}}">
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Description</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <textarea name="description" id="description" cols="30" rows="10">{{$item->description or ''}}</textarea>
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Logo</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                @include('helpers.input_file', [
                'name'=>'logo',
                'value'=>isset($item->logo)? $item->logo: '',
                ])
            </div>
        </div>
        <div class="row col-lg-12">
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Contact details</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <label class="form-comment">Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="" value="{{$item->phone or ''}}">
                <label class="form-comment">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="" value="{{$item->email or ''}}">
                <label class="form-comment">Fax</label>
                <input type="text" class="form-control" id="fax" name="fax" placeholder="" value="{{$item->fax or ''}}">
            </div>
        </div>
    </div>
    <div class="row col-lg-6 col-sm-12 form-block">
        <div class="row col-lg-12">
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Address</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <label class="form-comment">Country</label>
                <select name="country_id" id="country_id" class="form-control">
                    <option value="1" >Moldova</option>
                    <option value="">Moldova</option>
                    <option value="">Moldova</option>
                </select>
                <label class="form-comment">District</label>
                <select name="district_id" id="district_id" class="form-control">
                    <option value="1">Chisinau</option>
                    <option value="">Leova</option>
                </select>
                <label class="form-comment">Locality</label>
                <select name="locality_id" id="locality_id" class="form-control">
                    <option value="1">Moldova</option>
                    <option value=""></option>
                    <option value=""></option>
                    <option value=""></option>
                </select>
                <label class="form-comment">Street</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="" value="{{$item->address or ''}}">
            </div>
        </div>
    </div>
</form>
@endsection