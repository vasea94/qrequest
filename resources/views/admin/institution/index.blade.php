@extends('app')

@section('breadcrumbs')
    @include('helpers.breadcrumbs', [
        'actions'=>[
            [
                'name'=>'Add',
                'url'=>url('admin/institution/add'),
                'type'=>'add',
            ],
            [
                'name'=>'Edit',
                //'url'=>'add',
                'type'=>'edit'
            ]
        ],
        'title'=>'Manage institutions'
    ])
@endsection
@section('content')

<div class="row">
    @if(isset($institutions) && count($institutions))
        @foreach($institutions as $institution)
            <div class="col-lg-4">
                <div class="panel">
                    <div class="panel-body">
                        <div class="profile">
                            <div style="margin-bottom: 15px" class="row">
                                <div class="col-xs-12 col-sm-8">
                                    <h2>{{$institution->name or ''}}</h2>
                                    <p>
                                        <strong>{{trans('variables.about')}}:</strong> {{$institution->description or ''}}</p>
                                    <p>
                                        <strong>{{trans('variables.address')}}:</strong> {{$institution->address or ''}}</p>
                                    <p>
                                        <strong>{{trans('variables.phone')}}:</strong> {{$institution->phone or ''}}</p>
                                    <p>
                                        <strong>{{trans('variables.fax')}}:</strong> {{$institution->fax or ''}}</p>

                                </div>
                                <div class="col-xs-12 col-sm-4 text-center">
                                    <figure><img src="{{$institution->logo or ''}}" alt="" style="display: inline-block" class="height-1 img-responsive img-thumbnail img-rounded">
                                        <figcaption class="ratings"><p><a href="#"><span class="fa fa-star"></span></a><a href="#"><span class="fa fa-star"></span></a><a href="#"><span class="fa fa-star"></span></a><a href="#"><span class="fa fa-star"></span></a><a href="#"><span class="fa fa-star-o"></span></a></p></figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="row text-center divider">
                                <div class="col-xs-12 col-sm-4 emphasis">
                                    <h2>
                                        <strong>{{ count($institution->departments) }}</strong></h2>
                                        <a href="{{url('/admin/institution/'.$institution->id.'/department/')}}" class="center-block ">{{trans('variables.departments')}}</a>
                                </div>
                                <div class="col-xs-12 col-sm-4 emphasis">
                                    <h2>
                                        <strong>{{ count($institution->departments) }}</strong></h2>
                                    <a href="{{url('/admin/institution/'.$institution->id.'/department/')}}" class="center-block">{{trans('variables.users')}}</a>
                                </div>
                                <div class="col-xs-12 col-sm-4 emphasis">
                                    <h2>
                                        <strong>{{ count($institution->departments) }}</strong></h2>
                                    <a href="{{url('/admin/institution/'.$institution->id.'/department/')}}" class="center-block">{{trans('variables.functions')}}</a>
                                </div>
                            </div>
                            <div class="row separated">
                                <div class="col-xs-10 col-sm-6 ">
                                    <a href="#" class="btn btn-xs">Registration date <span class="badge">42</span></a>
                                </div>
                                    <div class="btn-group dropdown col-xs-2 col-sm-1">
                                    <button type="button" data-toggle="dropdown" class="btn-xs btn btn-orange dropdown-toggle">
                                        <span class="fa fa-gear"></span>&nbsp;
                                    </button>
                                    <ul role="menu" class="dropdown-menu pull-right text-left">
                                        <li><a href="#"><span class="fa fa-envelope"></span>&nbsp; Send an email</a></li>
                                        <li><a href="#"><span class="fa fa-list"></span>&nbsp; Add or remove from a list</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><span class="fa fa-warning"></span>&nbsp; Report this user for spam</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" role="button" class="btn disabled">Unfollow</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
@endsection