@extends('app')

@section('breadcrumbs')
    @include('helpers.breadcrumbs', [
        'actions'=>[
            [
                'name'=>'Add',
                'url'=>url('admin/action/add'),
                'type'=>'add',
            ],
        ],
        'title'=>'Manage available actions'
    ])
@endsection
@section('content')

<div class="">
    <table class="dataTable table">
        <thead>
            <td>Name</td>
            <td>Description</td>
        </thead>
        <tbody>
            @if(isset($items) && count($items))
                @foreach($items as $item)
                    <tr>
                        <td>{{$item->name or ''}}</td>
                        <td>{{$item->description or ''}}
                            <a href="{{url('admin/department/add/'.$department->id)}}" class="btn  pull-right settings">
                                <i class="fa fa-edit"></i>
                                Edit
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection