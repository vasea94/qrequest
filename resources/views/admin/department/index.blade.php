@extends('app')

@section('breadcrumbs')
    @include('helpers.breadcrumbs', [
        'actions'=>[
            [
                'name'=>'Add',
                'url'=>url('admin/department/add'),
                'type'=>'add',
            ],
        ],
        'title'=>'Manage departments'
    ])
@endsection
@section('content')

<div class="">
    <table class="dataTable table">
        <thead>
            <td>Name</td>
            <td>Description</td>
        </thead>
        <tbody>
            @if(isset($departments) && count($departments))
                @foreach($departments as $department)
                    <tr>
                        <td>{{$department->name or ''}}</td>
                        <td>{{$department->description or ''}}
                            <a href="{{url('admin/department/add/'.$department->id)}}" class="btn  pull-right settings">
                                <i class="fa fa-edit"></i>
                                Edit
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection