@extends('app')
@section('breadcrumbs')
    @include('helpers.breadcrumbs', [
        'actions'=>[
            [
                'name'=>'Add',
                'url'=>'add',
                'type'=>'add'
            ],
            [
                'name'=>'Save',
                // 'url'=>'save',
                'type'=>'save'
            ]
        ],
        'title'=>'Administer department'
    ])
@endsection
@section('content')
<form action="{{url('/admin/department/save/'.(isset($item->id)?$item->id : ''))}}" method="post"
      class="form-save" role="form">
    <div class="row col-lg-6 col-sm-12 form-block">
        <div class="row col-lg-12">
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Department name</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <input type="text" class="form-control" id="name" value="{{$item->name or ''}}" name="name" placeholder="{{trans('variables.enter_name')}}">
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Description</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <textarea name="description" id="description" cols="30" rows="10">{{$item->description or ''}}</textarea>
            </div>
        </div>
    </div>
</form>
@endsection