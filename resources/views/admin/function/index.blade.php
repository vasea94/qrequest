@extends('app')

@section('breadcrumbs')
    @include('helpers.breadcrumbs', [
        'actions'=>[
            [
                'name'=>'Add',
                'url'=>url('admin/function/add'),
                'type'=>'add',
            ],
        ],
        'title'=>'Manage functions'
    ])
@endsection
@section('content')

<div class="">
    <table class="dataTable table">
        <thead>
            <td>Name</td>
            <td>Description</td>
        </thead>
        <tbody>
            @if(isset($functions) && count($functions))
                @foreach($functions as $function)
                    <tr>
                        <td>{{$function->name or ''}}</td>
                        <td>{{$function->description or ''}}
                            <a href="{{url('admin/function/add/'.$function->id)}}" class="btn  pull-right settings">
                                <i class="fa fa-edit"></i>
                                Edit
                            </a></td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection