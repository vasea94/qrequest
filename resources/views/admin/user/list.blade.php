@extends('app')

@section('breadcrumbs')
    @include('helpers.breadcrumbs', [
        'actions'=>[
            [
                'name'=>'Add',
                'url'=>url('admin/user/add'),
                'type'=>'add',
            ],
        ],
        'title'=>'Manage users'
    ])
@endsection
@section('content')

<div class="">
    <table class="dataTable table">
        <thead>
            <td>Name</td>
            <td>Email</td>
            <td>Active</td>
        </thead>
        <tbody>
            @if(isset($items) && count($items))
                @foreach($items as $item)
                    <tr>
                        <td>{{$item->name or ''}}</td>
                        <td>{{$item->email or ''}}</td>
                        <td>{{(!$item->active) ? 'Inactive':'Active'}}
                            <a href="{{url('admin/user/add/'.$item->id)}}" class="btn  pull-right settings">
                                <i class="fa fa-edit"></i>
                                Edit
                            </a></td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection