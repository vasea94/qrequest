@extends('app')
@section('breadcrumbs')
    @include('helpers.breadcrumbs', [
        'actions'=>[
            [
                'name'=>'Add',
                'url'=>'add',
                'type'=>'add'
            ],
            [
                'name'=>'Save',
                // 'url'=>'save',
                'type'=>'save'
            ]
        ],
        'title'=>'Administer user'
    ])
@endsection
@section('content')
<form action="{{url('/admin/user/save/'.(isset($item->id)?$item->id : ''))}}" method="post"
      class="form-save" role="form">
    <div class="row col-lg-6 col-sm-12 form-block">
        <div class="row col-lg-12">
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">User name</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <input type="text" class="form-control" id="name" value="{{$item->name or ''}}" name="name" placeholder="{{trans('variables.enter_name')}}">
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Email</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <input type="text" class="form-control" id="email" value="{{$item->email or ''}}" name="email" placeholder="{{trans('variables.enter_email')}}" autocomplete="off">
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Password</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <input type="password" class="form-control" id="password" value="" name="password" placeholder="{{trans('variables.enter_password')}}" autocomplete="off">
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Status</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <label class="btn btn-default">
                    <input type="radio" id="active" name="active" value="2" {{(isset($item->active) && $item->active == 2)? 'checked': ''}}/> active
                </label>
                <label class="btn btn-default">
                    <input type="radio" id="active" name="active" value="1" {{(isset($item->active) && $item->active == 1)? 'checked': ''}}/> inactive
                </label>
            </div>
        </div>
    </div>
    <div class="row col-lg-6 col-sm-12 form-block">
        <div class="row col-lg-12">
            <p class="div-title">User rights</p>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">User rights</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <input type="text" class="form-control" id="name" value="{{$item->name or ''}}" name="name" placeholder="{{trans('variables.enter_name')}}">
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Email</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <input type="text" class="form-control" id="email" value="{{$item->email or ''}}" name="email" placeholder="{{trans('variables.enter_email')}}" autocomplete="off">
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Password</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <input type="password" class="form-control" id="password" value="" name="password" placeholder="{{trans('variables.enter_password')}}" autocomplete="off">
            </div>
            <div class="col-sm-3 col-lg-3">
                <label class="form-comment">Status</label>
            </div>
            <div class="form-group col-sm-9 col-lg-9">
                <label class="btn btn-default">
                    <input type="radio" id="active" name="active" value="2" {{(isset($item->active) && $item->active == 2)? 'checked': ''}}/> active
                </label>
                <label class="btn btn-default">
                    <input type="radio" id="active" name="active" value="1" {{(isset($item->active) && $item->active == 1)? 'checked': ''}}/> inactive
                </label>
            </div>
        </div>
    </div>

</form>
@endsection