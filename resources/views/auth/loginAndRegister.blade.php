@extends('app')
@section('content')
    <div class="row">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a data-target="#login" data-toggle="tab">Login</a></li>
            <li><a data-target="#register" data-toggle="tab">Register</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="login">
                <form id="login" action="{{url('auth/login')}}" method="post" class="col s12 l4 form-group form-save"
                      role="form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row col-lg-6 col-sm-12 form-block">
                        <div class="row col-lg-12">
                            <p class="div-title">{{trans('variables.log_in_with_existing_account')}}</p>
                            <div class="col-sm-3 col-lg-3">
                                <label class="form-comment">{{trans('variables.your_email_address')}}</label>
                            </div>
                            <div class="form-group col-sm-9 col-lg-9">
                                <input type="email" class="form-control" id="email" value="{{$item->name or ''}}" name="email" placeholder="{{trans('variables.email')}}">
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <label class="form-comment">{{trans('variables.your_password')}}</label>
                            </div>
                            <div class="form-group col-sm-9 col-lg-9">
                                <input type="password" class="form-control" id="password" value="{{$item->email or ''}}" name="password" placeholder="{{trans('variables.password')}}" autocomplete="off">
                            </div>
                            <div class="col-sm-9 col-lg-9">
                                <label class="btn btn-default" onclick="saveForm(this)" data-form-id="login">
                                    {{trans('variables.login')}}
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="register">
                <form action="{{url('request')}}" method="post" class="col s12 l4 form-group">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row col-lg-6 col-sm-12 form-block">
                        <div class="row col-lg-12">
                            <p class="div-title">{{trans('variables.create_new_account')}}</p>
                            <div class="col-sm-3 col-lg-3">
                                <label class="form-comment">User name</label>
                            </div>
                            <div class="form-group col-sm-9 col-lg-9">
                                <input type="text" class="form-control" id="name" value="{{$item->name or ''}}" name="name" placeholder="{{trans('variables.enter_name')}}">
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <label class="form-comment">Email</label>
                            </div>
                            <div class="form-group col-sm-9 col-lg-9">
                                <input type="text" class="form-control" id="email" value="{{$item->email or ''}}" name="email" placeholder="{{trans('variables.enter_email')}}" autocomplete="off">
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <label class="form-comment">Password</label>
                            </div>
                            <div class="form-group col-sm-9 col-lg-9">
                                <input type="password" class="form-control" id="password" value="" name="password" placeholder="{{trans('variables.enter_password')}}" autocomplete="off">
                            </div>
                        </div>
                        <div class="row col-lg-12">
                            <p class="div-title">{{trans('variables.create_new_account')}}</p>
                            <div class="col-sm-3 col-lg-3">
                                <label class="form-comment">User name</label>
                            </div>
                            <div class="form-group col-sm-9 col-lg-9">
                                <input type="text" class="form-control" id="name" value="{{$item->name or ''}}" name="name" placeholder="{{trans('variables.enter_name')}}">
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <label class="form-comment">Email</label>
                            </div>
                            <div class="form-group col-sm-9 col-lg-9">
                                <input type="text" class="form-control" id="email" value="{{$item->email or ''}}" name="email" placeholder="{{trans('variables.enter_email')}}" autocomplete="off">
                            </div>
                            <div class="col-sm-3 col-lg-3">
                                <label class="form-comment">Password</label>
                            </div>
                            <div class="form-group col-sm-9 col-lg-9">
                                <input type="password" class="form-control" id="password" value="" name="password" placeholder="{{trans('variables.enter_password')}}" autocomplete="off">
                            </div>
                            <div class="col-sm-9 col-lg-9">
                                <label class="btn btn-default" onclick="saveForm(this)" data-form-id="login">
                                    {{trans('variables.login')}}
                                </label>
                            </div>


                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
