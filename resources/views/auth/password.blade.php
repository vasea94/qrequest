<?php
    use Former\Facades\Former;
?>

@extends('layout')

@section('content')

    <div class="row">
        <div class="columns small-12 medium-6 medium-offset-3">
            <div class="panel">
                <h3>{{trans('variables.send_password_reset_link')}}</h3>

                {!! Former::horizontal_open() !!}

                {!! Former::text('Email') !!}

                {!! Former::actions()
                ->class('text-right')
                ->medium_primary_submit('Submit') !!}

                {!! Former::close() !!}

            </div>
        </div>

@endsection
