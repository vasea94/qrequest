@if(isset($institution) && isset($institution->departments) && count($institution->departments))
    @foreach($institution->departments as $departmentKey=>$department)
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#department{{$departmentKey}}" class=""><i class="fa fa-fw fa-arrows-v"></i>{{$department->name or ''}}<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="department{{$departmentKey}}" class="collapse">
                    @if(isset($department->functions) && count($department->functions))
                        @foreach($department->functions as $function)
                        <li>
                            <a href="#">{{$function->name}}</a>
                        </li>
                        @endforeach
                    @else
                        <li>
                            <a href="#">{{trans('variables.function_is_not_defined')}}</a>
                        </li>
                    @endif
                </ul>
            </li>
    @endforeach
@endif
{{--if is the same, the root--}}
@if(Auth::check() && Auth::user()->user_role_id == 1)
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#root"><i class="fa fa-fw fa-arrows-v"></i>Root<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="root" class="collapse">
            <li>
                <a href="{{url('/admin/institution')}}">Institutions</a>
            </li>
            <li>
                <a href="{{url('/admin/department')}}">Departments</a>
            </li>
            <li>
                <a href="{{url('/admin/function')}}">Functions</a>
            </li>
            <li>
                <a href="{{url('/admin/action')}}">Actions</a>
            </li>
            <li>
                <a href="{{url('/admin/user')}}">Users</a>
            </li>

        </ul>
    </li>
@endif